defmodule Curiosity do
  @moduledoc """
  Receives a set of instructions that specify where will the probes land and how
  should they explore the area and returns the estimated final position of each
  probe after applying the movement instructions
  """

  @typep bounding :: {east :: non_neg_integer, north :: non_neg_integer}
  @typep probe :: {x :: non_neg_integer, y :: non_neg_integer, direction :: (:north | :east | :south | :west)}

  @spec print_from_file(Path.t) :: no_return
  @doc """
  Loads the set of instructions from a file, executes them and print the probes
  position
  """
  def print_from_file(file), do: print_probes(load_from_file(file))

  @spec load_from_file(Path.t) :: [probe]
  @doc """
  Loads the set of instructions from a file, execute them and return the probes
  position
  """
  def load_from_file(file) do
    file
    |> File.stream!()
    |> calculate_positions()
  end

  @spec print_probes(probe | [probe]) :: no_return
  @doc """
  Prints `probes` in a friendly way
  """
  def print_probes(probes) do
    probes
    |> List.wrap()
    |> Enum.each(fn probe -> probe |> format() |> IO.puts() end)
  end

  @spec format(probe) :: String.t
  @doc """
  Returns the probe in a print friendly format
  """
  def format({x, y, direction}),
    do: "#{x} #{y} #{direction_str(direction)}"

  @spec calculate_positions([String.t]) :: [probe]
  @doc """
  Executes the instructions from `instruction_list` and return the list of probes
  where they are estimated to be after the conclusion 
  """
  def calculate_positions(instruction_list) do
    instruction_list
    |> Enum.reduce(
      {[], :nil, :bounding},
      fn
        string, {[], :nil, :bounding} ->
          b = bounding(string)
          {[], b, :probe}
        string, {probes, bounding, :probe} ->
          p = probe(string, bounding)
          {probes, bounding, p}
        string, {probes, bounding, probe} ->
          p = move_probe(string, probe, bounding)
          {[p|probes], bounding, :probe}
    end)
    |> elem(0) # We are expecting the first elem of the tuple (the probe list)
    |> Enum.reverse()
  end

  @spec bounding(String.t) :: bounding
  @doc """
  Defines the bounding limits of the area based on the received instructions
  """
  def bounding(string) do
    with \
      {east, string2} <- numeric_value(string),
      {north, _} <- numeric_value(string2),
    do: {east, north}
  end

  @spec probe(String.t, bounding) :: probe
  @doc """
  Turns an `instructions` string into the definition of a probe as long as it is
  inside the `bounding`
  """
  def probe(instructions, {bx, by}) do
    with \
      {x, str2} <- numeric_value(instructions),
      {y, str3} <- numeric_value(str2),
      dir = direction_value(str3)
    do
      if x <= bx and y <= by do
        {x, y, dir}
      else
        raise ArgumentError, message: "Probe starting position is outside the boundss"
      end
    end
  end

  @spec move_probe(String.t, probe, bounding) :: probe
  @doc """
  Executes the specified instructions to move the probe inside the boundings of
  the planet area and returns the final position of the probe

  Note that if a probe is instructed to move beyond the bounding area, that
  instruction will be ignored
  """
  def move_probe("M" <> instructions, p = {_, y, :north}, b = {_, y}),
    do: move_probe(instructions, p, b)
  def move_probe("M" <> instructions, {x, y, :north}, b = {_, _}),
    do: move_probe(instructions, {x, y + 1, :north}, b)
  def move_probe("M" <> instructions, p = {x, _, :east}, b = {x, _}),
    do: move_probe(instructions, p, b)
  def move_probe("M" <> instructions, {x, y, :east}, b = {_, _}),
    do: move_probe(instructions, {x + 1, y, :east}, b)
  def move_probe("M" <> instructions, p = {_, 0, :south}, b = {_, _}),
    do: move_probe(instructions, p, b)
  def move_probe("M" <> instructions, {x, y, :south}, b = {_, _}),
    do: move_probe(instructions, {x, y - 1, :south}, b)
  def move_probe("M" <> instructions, p = {0, _, :west}, b = {_, _}),
    do: move_probe(instructions, p, b)
  def move_probe("M" <> instructions, {x, y, :west}, b = {_, _}),
    do: move_probe(instructions, {x - 1, y, :west}, b)
  def move_probe("L" <> instructions, {x, y, :north}, b = {_, _}),
    do: move_probe(instructions, {x, y, :west}, b)
  def move_probe("L" <> instructions, {x, y, :east}, b = {_, _}),
    do: move_probe(instructions, {x, y, :north}, b)
  def move_probe("L" <> instructions, {x, y, :south}, b = {_, _}),
    do: move_probe(instructions, {x, y, :east}, b)
  def move_probe("L" <> instructions, {x, y, :west}, b = {_, _}),
    do: move_probe(instructions, {x, y, :south}, b)
  def move_probe("R" <> instructions, {x, y, :north}, b = {_, _}),
    do: move_probe(instructions, {x, y, :east}, b)
  def move_probe("R" <> instructions, {x, y, :east}, b = {_, _}),
    do: move_probe(instructions, {x, y, :south}, b)
  def move_probe("R" <> instructions, {x, y, :south}, b = {_, _}),
    do: move_probe(instructions, {x, y, :west}, b)
  def move_probe("R" <> instructions, {x, y, :west}, b = {_, _}),
    do: move_probe(instructions, {x, y, :north}, b)
  def move_probe("\n" <> _, p, _),
    do: p
  def move_probe("", p, _),
    do: p

  @spec numeric_value(String.t) :: {non_neg_integer, String.t}
  defp numeric_value(string, acc \\ "")
  defp numeric_value(" " <> string, acc),
    do: {String.to_integer(acc), string}
  defp numeric_value("\n" <> string, acc),
    do: {String.to_integer(acc), string}
  defp numeric_value("", acc),
    do: {String.to_integer(acc), ""}
  defp numeric_value(<< num :: utf8, str :: binary>>, acc) when num in ?0..?9,
    do: numeric_value(str,  acc <> << num >>)

  @spec direction_value(String.t) :: :north | :east | :south | :west
  defp direction_value("N" <> _),
    do: :north
  defp direction_value("E" <> _),
    do: :east
  defp direction_value("S" <> _),
    do: :south
  defp direction_value("W" <> _),
    do: :west
  defp direction_value(" " <> str),
    do: direction_value(str)

  @spec direction_str(:north | :east | :south | :west) :: String.t
  defp direction_str(:north), do: "N"
  defp direction_str(:east), do: "E"
  defp direction_str(:south), do: "S"
  defp direction_str(:west), do: "W"
end