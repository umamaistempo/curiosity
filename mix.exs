defmodule Curiosity.Mixfile do
  use Mix.Project

  def project do
    [
      app: :curiosity,
      version: "0.0.1",
      elixir: "~> 1.2",
      deps: deps]
  end
  def application do
    []
  end

  defp deps do
    []
  end
end
