defmodule CuriosityTest do
  use ExUnit.Case

  test "Bounding accept any non-neg integer" do
    bounding1 = Curiosity.bounding("5 5")
    bounding2 = Curiosity.bounding("50 5")
    bounding3 = Curiosity.bounding("50 50")
    bounding4 = Curiosity.bounding("987 123456")

    assert {5, 5} === bounding1
    assert {50, 5} === bounding2
    assert {50, 50} === bounding3
    assert {987, 123456} === bounding4
  end

  test "Invalid bounding values will break" do
    assert_raise FunctionClauseError, fn ->
      Curiosity.bounding("-5 5")
    end

    assert_raise FunctionClauseError, fn ->
      Curiosity.bounding("5 Foo 5")
    end
  end

  test "Probe init" do
    bounding = Curiosity.bounding("99 99")
    probe1 = Curiosity.probe("3 3 N", bounding)
    probe2 = Curiosity.probe("20 0 S", bounding)

    assert {3, 3, :north} === probe1
    assert {20, 0, :south} === probe2
  end

  test "Probe must be inside the bounding area" do
    bounding = Curiosity.bounding("99 99")

    assert_raise ArgumentError, fn ->
      Curiosity.probe("100 0 N", bounding)
    end
  end

  test "Invalid initial probe values break" do
    bounding = Curiosity.bounding("99 99")

    assert_raise FunctionClauseError, fn ->
      Curiosity.probe("10 -9 N", bounding)
    end
    assert_raise FunctionClauseError, fn ->
      Curiosity.probe("10 10 K", bounding)
    end
  end

  test "Probes will properly move according to instructions" do
    bounding = Curiosity.bounding("99 99")
    probe = Curiosity.probe("3 3 N", bounding)

    assert Curiosity.move_probe("MMLM", probe, bounding) === {2, 5, :west}
    assert Curiosity.move_probe("RMRM", probe, bounding) === {4, 2, :south}
  end

  test "Probes won't move beyond the bounding" do
    bounding = Curiosity.bounding("5 5")
    probe = Curiosity.probe("1 1 N", bounding)

    assert Curiosity.move_probe("MMMMMMMMMMMMMMMMMM", probe, bounding) === {1, 5, :north}
    assert Curiosity.move_probe("LMMMMMMMMMMMMMMMMMM", probe, bounding) === {0, 1, :west}
  end

  test "Formated output comes as expected" do
    input = "3 1 N"
    bounding = Curiosity.bounding("5 5")

    assert Curiosity.format(Curiosity.probe(input, bounding)) === input
  end

  test "Execute from file" do
    output = "test/input.txt" |> Curiosity.load_from_file() |> Enum.map(&Curiosity.format(&1))
    expected = ["1 3 N", "5 1 E"]

    assert output === expected
  end
end