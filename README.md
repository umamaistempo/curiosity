# Curiosity

**Curiosity** is an application whose main purpose is to receive an input with
analytical data about mars probes and output the expected geoposition of each
probe.

This application assumes that the data will be used solely to predict where will
the probes be located on the Mars surface after properly processing the input
commands

The application will receive a block of instructions (usually a file) containing
3 to several lines.

The first line should be the north-east point of the bounding box of the
available mars area. Eg: `5 5` would mean that the valid mars area
ranges from `{0, 0}` to `{5, 5}` being the first value the `X` in a
carthesian plane (in this case, akin to longitude) and the second value, the
`Y` in a carthesian plane (in this case, akin to latitude)

The following even lines (ie: 2, 4, 6, ...) are the definition of the starting
point of the probes. The line content should be 2 numbers and a letter in the
range of `[N, E, S, W]` (properly spaced and the letter **MUST** be
uppercased). Eg: `2 3 N` would mean that the probe starting point is the
geopoint `{2, 3}` and is facing **N**orth.

The following odd lines (ie: 3, 5, 7, ...) are the movement instructions to be
given to the previously defined probe. The line content should be a string of
letters in the range of `[L, M, R]` being `L` an instruction for the
probe to move 90º to the **L**eft, `R` an instruction for the probe to move
90º to the **R**ight and `M` an instruction for the probe to **M**ove forward.
Eg: `MMRMLMM`

It is assumed that if a probe is instructed to move outside the bounding box area
it shall ignore the command (eg: if the bounding area is `{5, 5}` and a probe
is already at `{5, 1}` and facing north, it will ignore any instruction to
move forward)